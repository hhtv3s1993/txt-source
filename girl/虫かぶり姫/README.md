# novel

- title: 虫かぶり姫
- title_zh: 書蟲公主
- author: 由唯
- illust:
- source: http://ncode.syosetu.com/n4942cw/
- source_comic: http://online.ichijinsha.co.jp/zerosum/comic/musikaburihime
- cover: https://images-na.ssl-images-amazon.com/images/I/81JT%2BtcdH1L.jpg
- publisher: syosetu
- date: 2019-05-25T20:09:00+08:00
- status: 連載
- novel_status: 0x0300

## illusts

- 喜久田ゆい
- 椎名咲月
- 

## publishers

- syosetu

## series

- name: 虫かぶり姫

## preface


```
　麗しの王子、クリストファー殿下の婚約者として過ごしてきた侯爵令嬢、エリアーナ・ベルンシュタイン。本好きが高じて彼女につけられたあだ名は、本の虫ならぬ「虫かぶり姫」。王子とは名ばかりの婚約関係だが、そんな彼女の耳にも最近とある噂が入ってくる。権力闘争をおさめた王子は、ようやく意中の姫君を妃に迎えられるとか。そして彼女はある日、王子が素の自分を噂のご令嬢に見せている現場を目撃する。婚約者のお役目はこれで終わり。なのに、なぜ胸が痛むのでしょう……。
```

## tags

- node-novel
- R15
- syosetu
- アイリス恋愛Ｆ大賞
- ラブコメ
- 天然
- 婚約破棄
- 恋愛
- 恋愛小説
- 悪役令嬢？
- 異世界[恋愛]
- 異世界〔恋愛〕

# contribute

- wodhy

# options

## downloadOptions

- noFilePadend: true
- filePrefixMode: 1
- startIndex: 1

## syosetu

- txtdownload_id:
- series_id:
- novel_id: n4942cw

## textlayout

- allow_lf2: true

# link

- [narou.nar.jp](https://narou.nar.jp/search.php?text=n4942cw&novel=all&genre=all&new_genre=all&length=0&down=0&up=100) - 小説家になろう　更新情報検索
- [小説情報](https://ncode.syosetu.com/novelview/infotop/ncode/n4942cw/)
- https://masiro.moe/forum.php?mod=viewthread&tid=13179
- https://shimo.im/docs/UgrhBHBGeuspdJ21/
- 漫畫第1話漢化（繁體）：[百度網盤](https://pan.baidu.com/s/1BtgYiPp43oAr0fP1BF_K_g) 提取碼：hnn4
- 


