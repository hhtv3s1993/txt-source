仿彿要就這麼飛上天空了。

超越人類極限的技巧……不，是更在這之上的《什麼》。

月折莉莎最初接觸到體育競技，是在二零四零年的夏季奧運會上。

當時，莉莎剛滿八歲，因為還沉迷於電視上的兒童向動畫，所以對暑假中父親一直占著客廳的平板電視感到非常不滿。但即使抱怨也只會惹怒脾氣不好的父親，只好每天看沒什麼興趣的奧運轉播。

無論是父親甚為狂熱的足球也好、柔道也好、田徑也好，莉莎都壓根不知道到底哪裡有趣，但是在那個人出現在畫面上的瞬間，她卻緊緊地盯著屏幕。

女子器材體操，項目是跳馬。穿著細長的白色緊身衣的選手，是年僅十幾歲的羅馬尼亞選手。

對此項目似乎沒什麼興趣的父親去了廁所，而母親則出門購物了，所以客廳只有莉莎一個人。電視內外都是一片寂靜，畫面中的女選手輕快地舉起右手，用特別的預備動作計算好時機後，開始在狹窄的助跑道上跑了起來。

她將上半身挺起，把手腕的揮動保持在最小限定，迅速地加速起來。用力地踏上跳板（這個名字是之後才知道的），用雙手往跳馬上一撐、跳躍。

那一瞬間，莉莎仿彿看見那位選手的背後伸出了白色的翅膀。

接著她馬上用雙手抱著膝蓋，開始向前轉體。一次，又一次，又一次……然後又一次。

在穩穩扎在墊子上落地之後，她寸步不動地在原地將身體伸直，舉高雙手。屏幕的兩邊傳來激烈的歡呼聲。

莉莎愣著張開嘴，看慢鏡頭回放的畫面看得出神了。

體育課的時候，班上的女生中只有莉莎能跳過六段的跳箱。雖然她曾悄悄地得意過，但是這位穿白色緊身衣的選手，比莉莎跳的還要高上好幾倍……一邊做著仿彿快要到達天空的跳躍，一邊用慢鏡頭也不能看清的速度旋轉著，最後完美地落地。簡直不像是人類的動作。

實況解說員用極度興奮的嘶啞聲，用《前手翻前轉三周》形容這個動作。加上一開始就向前翻了一次，實際上是四周轉體。用同樣的動作專題三周的技巧被命名為《Produnova*》，而轉體四周的動作還沒有名字。要說為什麼，正是因為這位羅馬尼亞的選手，是在世上首個展示出這一動作並成功了的選手。【譯註：俄羅斯選手伊蓮娜·普羅杜諾娃于1999年首次完成的動作，故以她的名字為此動作命名，又稱為“普娃跳”。這套動作能分解為前手翻上馬、兩圈前空翻下馬。也就是說，體操運動員要全速衝刺，然後翻上高空，在落地前完成2個前空翻。世上僅有5人曾完成這個動作，因危險性極高，體操界曾多次呼籲禁止這一動作。其難度分也是女子跳馬中最高的7.0分。】

之後的一天，這個動作以這名選手的名字命名了。

但是在那之後，不管是二零四四年的奧林匹克運動會，還是其他的大賽上，跳馬項目裡再也沒有出現能成功完成四周轉體的選手。包括以其名命名的本人也沒有成功。因為她在展示出這讓莉莎著迷的動作後右腳腳掌骨折，就這樣退役了。

那位選手和動作的名字是《Racovi··》。

這是驅使莉莎走上體操選手道路的動作。

在尚且年幼的那天，她在心裡發誓有朝一日一定要把那個動作跳出來——。
