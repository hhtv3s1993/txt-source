在紮拉前輩的背上，似乎有10個空洞說著『管子插在這裡哦！』。
並不能看見身體裡面，裡面好像是黑色的。

「這是什麼？」

為了讓提亞教授聽得懂，我向她進行了說明。

「要是我問你，你也會很為難吧。有沒有可以插入的「東西」可以試試？」

的確如此。
暫且先把洞放一邊，從伸向地面紮著的管子開始擺弄。順帶一提，有18根紮在地上，到中途有4根，共計22根。

我用透明結界握住其中一根，用從地面拉出的印象使勁拉住。

「咿~呀！？」

紮拉前輩發出了奇怪的聲音。
不管怎樣，我正在使勁的拉。管子還拔不出來。

「喂、等、啊、啊啊~啊啊啊~~~、哈嗯~、嗯~、啊~、不……」

總覺得漸漸地變成了色情的聲音喲。
她不知不覺四肢著地，蜷縮著身體。

但是果然只是延伸了，卻看不到終點。和提亞教授時一樣。

這次決定下定決心，試著切看看。
做了尖銳的刀刃狀結界，切向其中一根管子。

「呀~嗯！？」

伸長的管子。戰戰兢兢的前輩。
無論做多少次也剪不斷。每次都會有不能讓孩子們聽到的色情聲音響起。

「魔法等級會不會下降？」

「沒聽說過呢。」

我想那是不可能的。如果能這樣做，就能抓住敵人的背後，一個勁地降低他們的魔法等級，削弱他們。真遺憾。

那麼，如果盡是些過分的事情（自覺）的話感到很抱歉，也提高一下她的魔法等級吧。
延伸到中途的4根中，我拉著兩根連接到地面。

「啊啊！……哼，哼！」

紮拉前輩趴了一會兒，顫抖了一下。

「那孩子是不是暈過去了？」提亞教授說。

分身啪嗒啪嗒地拍著紮拉前輩的後腦勺說：「真的啊」。

前輩沒有反應。
看著她的臉，從她張開的嘴裡流著口水，眼睛半張開翻白眼。

怎麼辦？正煩惱的時候，突然注意到了，前輩。

「為什麼魔法水等上升了！？而且還有兩個！」

她一下子站了起來，像我靠近。之前。還隱藏起來。她現在裸著上身。

「唔……咦，我的頭……搖搖晃晃的……」

但是她搖搖晃晃，剛要坐下就接住了。

「最好不要勉強。我也有過這樣的經驗，但是身體應該有異常吧。頭腦特別亂吧？好像一口氣提高了兩個等級呢。」

提亞教授把目光轉向這邊。
雖然好像討厭紮拉前輩，但是站在同樣的立場上同情她。

這裡我也應該溫柔的對待她吧。

「休息一會兒就好了。接下來就從這裡開始。」

「你是鬼啊！？」
「你還在幹嗎！？你以為我是什麼啊……」

那是實驗……嗯，對不起。

但是呢，因為現在想起了點事。
我想試試那個—。怎麼辦呢？

在這時，紮拉前輩披上外套，以體育座遮著大大的胸部休息的時候，我偷偷地向提亞教授傳達了自己的想法。

「經常想到那樣的事。嗯~，雖然有嘗試的價值，但是不管怎麼樣都不知道哦？」

把責任全部推給我的風格。我雖然不討厭，但我已經不想當教師了。

但是，既然不知道會發生什麼，就應該建立萬全的體制吧。

因此，為了以防萬一，呼籲了可靠的醫療人員。

「嗯，知道了。如果有什麼事的話，給這個女人施上治癒魔法就好了」 

她是擅長水系魔法的龍娘，麗莎老師。全身濕透，襯衣般的白襦袢緊貼在身上。但是，瞬間魔法般地把水放飛後馬上乾了。真不愧是。

順便叫了芙蕾。滿身是水。
但是、咕嚕咕嚕！物理性地飛水一個勁地，這邊濕潤的那樣安定了。

「原來如此，我是讓這個女人伏在地上，讓她動彈不得啊。」

「只是希望她不要因為抓狂而受傷。不要從一開始就用力壓住她。」

讓芙蕾鎮靜下來。嘛，讓男人的我去做，前輩也很討厭吧，沒辦法啊。

「做得太漂亮了……。雖然我還是不安。」

雖然這麼說，紮拉前輩還是拿著上衣坐著背對著他。
不管怎麼說都是合作性的。到現在為止一次也沒說『停止』。我也要對她的勇氣表示敬意，安全第一。

「那，我就動手吧。」

「突然什麼？卑躬屈膝真噁心。」

哈哈，妳這個笨蛋……我不會手下留情的！

我雖然有點生氣，但是重新振作起來。因為不能玩啊。

目不轉睛地看著紮拉前輩的背影。
雖然有十個孔，但像提亞教授說的「能插入的東西」什麼都沒有——你覺得嗎？

有這個吧。

我把一直伸到半路的兩根管子中的一根，一個勁兒地扭曲了。

是啊，我考慮著別把這傢伙插進洞裏！

……但是這個，沒問題吧？魔力逆流之類的感覺，會不會造成無法挽回的事情呢？

即使那樣，如果，如果。
如果插進去的管子，拿不出來呢？
即使麗莎施加了治癒魔法，但如果一直處於這種狀態的話，總有一天生命就會消失。

真讓人猶豫。
這個人並沒有對我和家人造成危害。將如此年輕的生命白白浪費掉，並非我的本意。

「那個，也許會死，可以嗎？」

「別說可怕的話！」

是吧ー。

「……但是沒關係。雖然不知道你要做什麼，但我已經做好覺悟了。要幹就趕快幹吧。」

是什麼把她逼到這種地步？
完全不明白，不過，因為她本人說著『做』，所以我也不會客氣了。

哢嚓哢嚓。

真的發出了「卡進去了」的聲音。

「——啊！？」

紮拉前輩發出無法發出聲響的悲鳴，顫抖著。翻著白眼，從嘴裡吐出泡沫，真糟糕！

「沒關係。沒有肉體上的損傷。」

但是，麗莎老師卻冷靜地診察了。

「我想她只是被強烈的刺激驚嚇到暈過去了。」

她的大腦沒有受損吧？

「比起這個，哈特君，能告訴我發生了什麼嗎？」

提亞教授她們看不見。還挺在意的。
我需要說明我所看到的一切。

「插進去之後，那根管子就分成了兩條。」

在後背的兩個地方相連的管子，自然地從中間斷開了。
結果，沒連接到地面的管子新增了一根。
總而言之。

「最大的魔法等級上升了！？」

我對提亞教授的驚訝聲，點頭，

「不愧是哈特大人！這已經是神的領域了！」
「不，也許不只於此？」

芙蕾和麗莎都非常吃驚——。 